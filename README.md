# Installing TEI environment

TEI versions :
- TEI P5 version 4.5.0 https://github.com/TEIC/TEI/releases/tag/P5_Release_4.5.0
- Stylesheets version 7.54.0 https://github.com/TEIC/Stylesheets/releases/tag/v7.54.0

## Debian / Ubuntu

```
sudo apt-get install ant
mkdir tei-c
cd tei-c

git clone https://github.com/TEIC/TEI.git ./TEI
cd TEI
git checkout -b branch/P5_Release_4.5.0 P5_Release_4.5.0
cd ..
git clone https://github.com/TEIC/Stylesheets.git ./Stylesheets
cd Stylesheets
git checkout -b branch/v7.54.0 v7.54.0 
# Install the stylesheets
make PREFIX=/ABS_PATH/tei-c install

```
source: https://tei-c.org/guidelines/p5/using-the-tei-github-repository/


# ODD chaining

The elements of the Commons Publishing TEI are defined in `odd/commons-publishing.odd`

Metopes and OpenEdition may use additional elements in their own environment. For this reason, we provide 2 odds, based on `odd/commons-publishing.odd` and using the ODD Chaining principle (http://teic.github.io/PDF/howtoChain.pdf)
- `odd/commons-publishing-openedtion.odd`
- `odd/commons-publishing-metopes.odd`


# Compiling rng and documentation

## Create compiled odd 

Required for odd chaining: `commons-publishing_compiled.odd` is included in both odds::
- `odd/commons-publishing-openedtion.odd`
- `odd/commons-publishing-metopes.odd`


```
cd tei-c/Stylesheets
./bin/teitoodd /PATH_TO/schema-tei/odd/commons-publishing.odd /PATH_TO/schema-tei/odd/commons-publishing_compiled.odd
```

## Create commons-publishing.rng
```
./bin/teitorelaxng /PATH_TO/schema-tei/odd/commons-publishing.odd /PATH_TO/schema-tei/rng/commons-publishing.rng 
```

## Create commons-publishing-openedition.rng
```
./bin/teitorelaxng /PATH_TO/schema-tei/odd/commons-publishing-openedition.odd /PATH_TO/schema-tei/rng/commons-publishing-openedition.rng 
```

## Create commons-publishing-metopes.rng
```
./bin/teitorelaxng /PATH_TO/schema-tei/odd/commons-publishing-metopes.odd /PATH_TO/schema-tei/rng/commons-publishing-metopes.rng 
```

## Create Openedition ODD documentation
```
cd tei-c/Stylesheets
java -jar lib/saxon10he.jar -s:/PATH_TO/schema-tei/odd/commons-publishing-openedition.odd -o:/PATH_TO/schema-tei/odd/commons-publishing-openedition-compiled.odd -xsl:odds/odd2odd.xsl doclang=en defaultSource=/PATH_TO/tei-c/TEI/P5/p5subset.xml
java -jar lib/saxon10he.jar -s:/PATH_TO/schema-tei/odd/commons-publishing-openedition-compiled.odd -o:/PATH_TO/schema-tei/odd/commons-publishing-openedition-lite.odd -xsl:odds/odd2lite.xsl doclang=en defaultSource=/PATH_TO/tei-c/TEI/P5/p5subset.xml
java -jar lib/saxon10he.jar -s:/PATH_TO/schema-tei/odd/commons-publishing-openedition-lite.odd -o:/PATH_TO/schema-tei/docs/source/_static/commons-publishing-openedition-en.html -xsl:html/html.xsl doclang=en defaultSource=/PATH_TO/tei-c/TEI/P5/p5subset.xml cssSecondaryFile=_static/css/doc.css

rm /PATH_TO/schema-tei/odd/commons-publishing-openedition-compiled.odd /PATH_TO/schema-tei/odd/commons-publishing-openedition-lite.odd
```


## Create Metopes ODD documentation
```
cd tei-c/Stylesheets
java -jar lib/saxon10he.jar -s:/PATH_TO/schema-tei/odd/commons-publishing-metopes.odd -o:/PATH_TO/schema-tei/odd/commons-publishing-metopes-compiled.odd -xsl:odds/odd2odd.xsl doclang=en defaultSource=/PATH_TO/tei-c/TEI/P5/p5subset.xml 
java -jar lib/saxon10he.jar -s:/PATH_TO/schema-tei/odd/commons-publishing-metopes-compiled.odd -o:/PATH_TO/schema-tei/odd/commons-publishing-metopes-lite.odd -xsl:odds/odd2lite.xsl doclang=en defaultSource=/PATH_TO/tei-c/TEI/P5/p5subset.xml 
java -jar lib/saxon10he.jar -s:/PATH_TO/schema-tei/odd/commons-publishing-metopes-lite.odd -o:/PATH_TO/schema-tei/docs/source/_static/commons-publishing-metopes-en.html -xsl:html/html.xsl doclang=en defaultSource=/PATH_TO/tei-c/TEI/P5/p5subset.xml cssSecondaryFile=_static/css/doc.css

rm /PATH_TO/schema-tei/odd/commons-publishing-metopes-compiled.odd /PATH_TO/schema-tei/odd/commons-publishing-metopes-lite.odd
```


