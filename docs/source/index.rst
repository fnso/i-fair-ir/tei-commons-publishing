TEI Commons Publishing
===============================================

.. note::

      Les sources ODD et les schémas de validation sont disponibles sur le dépôt git suivant : https://git.unicaen.fr/fnso/i-fair-ir/tei-commons-publishing.


Le format TEI Commons Publishing contient tous les éléments communs aux environnements Métopes et OpenEdition. Cependant des différences d'usages nous ont conduit à décliner des versions distinctes du schéma pour :

 - Lodel 2 (OpenEdition Books)
 - Métopes



.. toctree::
   :maxdepth: 1  
   :caption: Documentation

   odd_doc.rst
   usage_oeb.rst
   usage_oej.rst
   usage_metopes.rst
   examples.rst
   compatibility.rst
   download.rst
   license.rst


La première version de ce format TEI été élaborée par `OpenEdition <https://www.openedition.org>`_, `Metopes <http://www.metopes.fr>`_, le `Certic <https://www.certic.unicaen.fr>`_ et le `Pôle Document numérique (MRSH de Caen) <https://mrsh.unicaen.fr/pluridisciplinaire/poles-pluridisciplinaires/pole-document-numerique/>`_ dans le cadre du projet `I-FAIR-IR <https://www.ouvrirlascience.fr/implementation-des-principes-fair-dans-les-infrastructures-de-recherche/>`_ financé par le Fonds National pour la Science Ouverte.
	
.. image:: _static/metopes.png

.. image:: _static/certic.png

.. image:: _static/mrsh.png

.. image:: _static/openedition.png


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
