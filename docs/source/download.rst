Télécharger le schéma TEI
===============================================

Dernières versions :

- odd https://git.unicaen.fr/fnso/i-fair-ir/tei-commons-publishing/-/tree/master/odd

  - commons-publishing-metopes.odd
  - commons-publishing-openedition.odd

- relaxNG : https://git.unicaen.fr/fnso/i-fair-ir/tei-commons-publishing/-/tree/master/rng

  - commons-publishing-metopes.rng
  - commons-publishing-openedition.rng





