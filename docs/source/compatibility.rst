Compatibilité 
===============================================

Le format TEI Commons Publishing contient tous les éléments communs aux environnements Métopes et OpenEdition. Cependant des différences d'usages nous ont conduit à décliner des versions distinctes du schéma pour :

 - Lodel 2 (OpenEdition Books)
 - Métopes


Compatibilité Lodel   
---------------------------

- Le format TEI Commons Publishing est compatible avec Lodel 2.

- Pour la conversion de fichiers textes (.docx, .odt) au format TEI Commons Publishing pour Lodel 2, utiliser le pipeline ``doc2tei`` version 0.0.87 avec Circé (voir https://git.unicaen.fr/fnso/i-fair-ir/circe-server)

- Le format TEI compatible avec Lodel 1 reste le format tei.openedition. Il est documenté à cette adresse : https://tei-openedition.readthedocs.io


