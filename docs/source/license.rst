Licence 
#########################################################

Le schéma TEI Commons Publishing est distribué sous Licence CeCCIL-B 

- `Licence CeCCILL-B français <http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html>`_
- `CeCCILL-B License english <http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_


