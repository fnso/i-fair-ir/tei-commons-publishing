Documentation d'usage OpenEdition Books
========================================================

TeiHeader
---------

fileDesc
~~~~~~~~

L’élément ``fileDesc`` contient les métadonnées descriptives du document TEI
organisées dans les éléments suivants :

titleStmt
^^^^^^^^^

Titres, sous-titres, surtitres, titres traduits
'''''''''''''''''''''''''''''''''''''''''''''''

XPath :

-  Titre : ``/TEI/teiHeader/fileDesc/titleStmt/title[@type="main"]``

-  Sous-titre : ``/TEI/teiHeader/fileDesc/titleStmt/title[@type="sub"]``

-  Surtitre : ``/TEI/teiHeader/fileDesc/titleStmt/title[@type="sup"]``

-  Titres traduits : ``/TEI/teiHeader/fileDesc/titleStmt/title[@type="alt"
   and @xml:lang]``

Recommandations d’usage :

-  pas de saut de ligne (``lb``), un seul élément ``title`` par type.

-  en bas de casse sauf initiale, non terminés par un point.

-  pour les titres traduits l’attribut ``xml:lang`` est obligatoire avec un
   valeur au format ISO 639-1.

Exemple : 

.. code-block:: xml

  <teiHeader>
   <fileDesc>
    <titleStmt>
     <title type="main">Titre principal du document</title>
     <title type="sup">Surtitre</title>
     <title type="sub">Sous-titre</title>
     <title type="trl" xml:lang="en">Title translated into English</title>
    </titleStmt>
   </fileDesc>
  </teiHeader>


Auteurs, contributeurs
''''''''''''''''''''''

XPath :

-  Auteur : ``/TEI/teiHeader/fileDesc/titleStmt/author[@role="aut"]``

-  Traducteur (auteur) : ``/TEI/teiHeader/fileDesc/titleStmt/author[@role="trl"]``

-  Traducteur : ``/TEI/teiHeader/fileDesc/titleStmt/editor[@role="trl"]``

-  Éditeur scientifique : ``/TEI/teiHeader/fileDesc/titleStmt/editor[@role="edt"]``

-  Collaborateur : ``/TEI/teiHeader/fileDesc/titleStmt/editor[@role="ctb"]``

Nom, prénom et description (XPath relatifs aux XPath précédents qui
identifient les différents types de contributeurs) :

-  Prénom : ``./persName/forename``

-  Nom : ``./persName/surname``

-  ORCID : ``./idno[@type="ORCID"]``

-  IDREF : ``./idno[@type="IDREF"]``

-  Affiliation : ``./affiliation/orgName``

-  Biographie : ``./note[@type="biography"]``

-  Courriel : ``./email``

Recommandations d’usage : 

-  Possibilité d’indiquer plusieurs auteurs, traducteurs, etc. pour le document.

-  Possibilité d'utiliser l'élement `name` à la place de `persName`, dans ce cas on indique le nom complet sous la forme "Prénom Nom", Lodel divise la chaine de caractères en 2 champs distincts Nom et Prénom en se basant sur la dernière espace.

-  Attention à la casse et à l’orthographe pour éviter les doublons dans
   les index.

-  Ne pas confondre ``affiliation/orgName`` qui doit contenir uniquemement le
   nom de l’organisation à laquelle l’auteur est affilié avec ``note[@type="biography"`` qui peut contenir un ou plusieurs
   paragraghe présentant l’auteur ou le contributeur.

-  On utilise généralement l’élément ``editor`` pour le traducteur, mais
   lorsqu’il est considéré comme auteur du texte, on peut utiliser
   l’élément ``author`` (avec, dans les deux cas, l’attribut ``@role=‘trl’``).

- Pour les identifiants ORCID et IDREF, il faut renseigner uniquement l'identifiant et non une URL complète. Exemples :

  - ORCID : 0000-0000-0000-0000

  - IDREF : 111222333

Exemple : 

.. code-block:: xml

  <titleStmt>
    <!-- [...] -->
     <author role="aut">
      <persName>
       <forename>Prénom_1</forename>
       <surname>Nom_1</surname>
      </persName>
      <idno type="ORCID">0000-0000-0000-0000</idno>
      <idno type="IDREF">111222333</idno>
      <affiliation>
       <orgName>CNRS</orgName>
      </affiliation>
      <note type="biography">
       <p>Premier paragraphe de biographie de l'auteur <hi rend="italic">(peut contenir des mises en forme)</hi>
       </p>
       <p>Deuxième paragraphe</p>
      </note>
      <email>prenom_1.nom_1@cnrs.fr</email>
     </author>
     <author role="aut">
      <name>Prénom_2 Nom_2</name>
     </author>
     <editor role="trl">
      <persName>
       <forename>Prénom_traducteur</forename>
       <surname>Nom_traducteur</surname>
      </persName>
      <idno type="IDREF">000000000</idno>
      <affiliation>
       <orgName>Université Aix-Marseille</orgName>
      </affiliation>
     </editor>
     <editor role="edt">
      <persName>
       <forename>Prénom_éditeur_scientifique</forename>
       <surname>Nom_éditeur_scientifique</surname>
      </persName>
     </editor>
     <editor role="ctb">
      <persName>
       <forename>Prénom_collaborateur</forename>
       <surname>Nom_collaborateur</surname>
      </persName>
     </editor>
    <!-- [...] -->
    </titleStmt>

Funder
''''''

OpenEdition permet de renseigner avec l’élément ``funder`` le ou les
financeurs qui ont permis cette publication, uniquement sur la base du
référentiel `Funder
Registry <https://www.crossref.org/services/funder-registry/>`__.

Le contenu de cet élément ``funder`` doit impérativement avoir été renseigné
à partir du référentiel Funder Registry.

XPath :

-  DOI du financeur : ``/TEI/teiHeader/fileDesc/titleStmt/funder/idno[@type="funder_registry"
   and not(@subtype="grant_number")]``

-  Nom du financeur : ``/TEI/teiHeader/fileDesc/titleStmt/funder/orgName[@type="funder_registry"]``

-  Grant number : ``/TEI/teiHeader/fileDesc/titleStmt/funder/idno[@type="funder_registry"
   and @subtype="grant_number"]``

Recommandations d’usage :

-  Si plusieurs financeurs, répéter l’élément ``funder``.

-  Renseigner uniquement le DOI du financeur (sans http://doi.org).

-  Le Grant number peut contenir n’importe quel identifiant, pas
   obligatoirement un DOI.

Exemple : 

.. code-block:: xml

  <funder>
   <idno type="funder_registry">10.13039/501100001871</idno>
   <orgName type="funder_registry">Fundação para a Ciência e a Tecnologia</orgName>
   <idno type="funder_registry"
       subtype="grant_number">PTDC/2006</idno>
  </funder>

publicationStmt
^^^^^^^^^^^^^^^

Éditeur, distributeur
'''''''''''''''''''''

XPath :

-  Éditeur : ``/TEI/teiHeader/fileDesc/publicationStmt/publisher``

-  Distributeur : ``/TEI/teiHeader/fileDesc/publicationStmt/distributor``

Recommandations d’usage :

-  Utilisés dans la TEI produite en sortie de la plateforme uniquement.

-  Valide mais ignoré lors de l’import dans Lodel.

.. note::

   L'export TEI est en cours de développement sur la plateforme OpenEdition Books

..

Exemple : 

.. code-block:: xml

  <publicationStmt>
   <!-- [...] -->
   <publisher>Nom de l'éditeur</publisher>
   <distributor>OpenEdition</distributor>
   <!-- [...] -->
  </publicationStmt>


Identifiants du document
''''''''''''''''''''''''

XPath :

-  Numéro du document : ``/TEI/teiHeader/fileDesc/publicationStmt/idno[@type="documentnumber"]``

-  DOI : ``/TEI/teiHeader/fileDesc/publicationStmt/idno[@type="DOI"]``

-  Handle : ``/TEI/teiHeader/fileDesc/publicationStmt/idno[@type="HANDLE"]``

-  URL : ``/TEI/teiHeader/fileDesc/publicationStmt/idno[@type="URL"]``

Recommandations d’usage :

-  Numéro du document : information éditoriale affichée dans la
   référence électronique du document, utilisé pour faciliter la
   citation des documents électroniques.

-  URL, HANDLE, DOI : utilisés dans la TEI produite en sortie de la plateforme
   uniquement.

Exemple : 

.. code-block:: xml

  <publicationStmt>
   <!-- [...] -->
   <idno type="documentnumber">24</idno>
   <idno type="URL">https://books.openedition.org/oep/128</idno>
   <idno type="DOI">10.4000/books.oep.128</idno>
   <idno type="HANDLE">20.500.13089/fj</idno>
   <!-- [...] -->
  </publicationStmt>

Date
'''''

XPath :


-  Date de publication  : ``/TEI/teiHeader/fileDesc/publicationStmt/idno[@type="published"]``

Recommandations d’usage :

-  Date au format JJ/MM/AAAA.

-  La date de publication est utilisée dans la TEI produite
   en sortie de la plateforme uniquement. Elle est définie par Lodel à
   la date où le document est effectivement publié sur OpenEdition.

Exemple :

.. code-block:: xml

  <publicationStmt>
   <!-- [...] -->
   <date type="published">2021-02-01</date>
   <!-- [...] -->
  </publicationStmt>


Licence
'''''''

XPath :

-  Licence : ``/TEI/teiHeader/fileDesc/publicationStmt/availability/licence``

Recommandations d’usage :

-  Utilisés dans la TEI produite en sortie de la plateforme uniquement.

Exemple :

.. code-block:: xml

  <publicationStmt>
   <!-- [...] -->
   <availability>
    <licence target="https://creativecommons.org/licenses/by-nc-nd/4.0/">CC-BY-NC-ND-4.0</licence>
   </availability>
   <!-- [...] -->
  </publicationStmt>

sourceDesc
~~~~~~~~~~

L’élément ``sourceDesc`` contient les informations relatives au document
source qui a servi à produire ce document TEI. Il prendra un sens
différent selon l’usage du document TEI :

-  à l’import dans Lodel, ``sourceDesc`` contiendra les métadonnées
   relatives à l’édition papier le cas échéant.

-  à l’export ``sourceDesc`` contiendra les métadonnées du contexte de
   publication sur OpenEdition (sur la revue, le numéro, le livre…).

.. note::

   L'export TEI est en cours de développement sur la plateforme OpenEdition Books

..


biblStruct/analytic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’élément ``biblStruct`` -> ``analytic`` décrit les métadonnées du document
courant. Il est utilisé uniquement dans l'export TEI OpenEdition.

XPath :

-  Titre : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/title[@level="a" and @type="main"]``

-  Sous-titre : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/title[@level="a" and @type="sub"]``

-  Surtitre : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/title[@level="a" and @type="sup"]``

-  Titres traduits : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/title[@level="a" and @type="alt" and @xml:lang]``

-  Auteur : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/author[@role="aut"]``

-  Traducteur : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/editor[@role="trl"]``

-  Éditeur scientifique : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/editor[@role="pbl"]``

-  Collaborateur : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/analytic/editor[@role="cbt"]``


biblStruct/monogr
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’élément ``biblStruct`` -> ``monogr`` contient les métadonnées relatives à
l’environnement de publication du document TEI (livre).

XPath :

-  Titres

	*  Titre du livre : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/title[@level="m"]``

	*  Titre traduit du livre : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/title[@level="m" and @type="alt" and xml:lang]``

-  Identifiants

	*  ISBN électronique : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/idno[@type="eISBN"]``

	*  ISBN édition papier : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/idno[@type="pISBN"]``

	*  URL du livre : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/idno[@type="URL" and @subtype="book"]``

	*  DOI du livre : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/idno[@type="DOI" and @subtype="book"]``

-  Informations sur l’édition papier

	*  Pagination de l’édition papier : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/imprint/biblScope[@unit="page"]``

	*  Année d'édition : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/imprint/date[@type="published"]``

	*  Éditeur : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/monogr/imprint/publisher``


Pour l’import sur OpenEdition Books, seus les éléments suivants sont utilisables :

	*  Pagination de l’édition papier : renseignée en chiffres romains (V-XXV) ou en chiffres arabes (5-25), sans les mentions p. ou pp.

biblStruct/series
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Utilisé dans l’Export TEI OpenEdition seulement, l’élément ``biblStruct`` ->
``series`` contient les métadonnées relatives à l’environnement de
publication du document TEI (collection).

XPath :

-  Titre de la collection : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/series/title[@level="s"]``

-  Titre traduit de la collection : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/series/title[@level="s" and @type="alt" and xml:lang]``

-  ISSN électronique de la collection : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/series/idno[@type="eISSN"]``

-  ISSN papier de la collection : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/series/idno[@type="pISSN"]``

-  URL de la collection : ``/TEI/teiHeader/fileDesc/sourceDesc/biblStruct/series/idno[@type="URL"]``

Exemples biblStruct
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Exemple de chapitre de livre (export OE) :

.. code-block:: xml

  <sourceDesc>
   <biblStruct type="chapter">
    <analytic>
     <title level="a" type="main">Titre principal du document</title>
     <title level="a" type="sup">Surtitre</title>
     <title level="a" type="sub">Sous-titre</title>
     <title level="a" type="trl" xml:lang="en">English Translated Title</title>
     <author role="aut">
      <persName>
       <forename>Prénom_1</forename>
       <surname>Nom_1</surname>
      </persName>
      <idno type="ORCID">0000-0000-0000-0000</idno>
      <idno type="IDREF">111222333</idno>
      <affiliation>
       <orgName>CNRS</orgName>
      </affiliation>
      <note type="biography">
       <p>Premier paragraphe de biographie de l'auteur <hi rend="italic">(peut contenir des mise en forme)</hi>
       </p>
       <p>Deuxième paragraphe</p>
      </note>
      <email>prenom_1.nom_1@cnrs.fr</email>
     </author>
     <author role="aut">
      <name>Prénom_2 Nom_2</name>
     </author>
     <editor role="trl">
      <persName>
       <forename>Prénom_traducteur</forename>
       <surname>Nom_traducteur</surname>
      </persName>
      <idno type="idref">000000000</idno>
      <affiliation>
       <orgName>Université Aix-Marseille</orgName>
      </affiliation>
     </editor>
     <editor role="edt">
      <persName>
       <forename>Prénom_éditeur_scientifique</forename>
       <surname>Nom_éditeur_scientifique</surname>
      </persName>
     </editor>
     <editor role="ctb">
      <persName>
       <forename>Prénom_collaborateur</forename>
       <surname>Nom_collaborateur</surname>
      </persName>
     </editor>
    </analytic>
    <monogr>
     <title level="m">Titre du livre dans lequel le chapitre est paru</title>
     <idno type="pISBN">ISBN de l'édition papier du livre</idno>
     <idno type="eISBN">ISBN de l'édition électronique du livre</idno>
     <idno type="DOI" subtype="book">DOI du livre</idno>
     <idno type="URL" subtype="book">URL du livre</idno>
     <imprint>
      <publisher>Nom de l'éditeur</publisher>
      <biblScope unit="page">13-22</biblScope>
      <date type="published">2021</date>
     </imprint>
    </monogr>
    <series>
     <title level="s">Titre de la collection dans laquelle est paru le livre</title>
     <idno type="pISSN">ISSN de la collection</idno>
     <idno type="url">URL de la collection</idno>
    </series>
   </biblStruct>
  </sourceDesc>
..

encodingDesc
~~~~~~~~~~~~~~~~~~~~

appInfo
^^^^^^^

L’élément ``appInfo`` permet de décrire la ou les applications utilisées
pour produire ce document. Cet élément est répatable.

Exemple de chapitre de livre (export OE) :

.. code-block:: xml

  <encodingDesc>
   <appInfo>
    <application version="0.0.0"
        ident="circe">
     <label>Nom de l'application qui a servi à produire ce fichier</label>
     <desc>Description de l'application</desc>
     <ref target="https://url_source_app"/>
    </application>
   </appInfo>
  </encodingDesc>

tagsDecl
^^^^^^^^

L’élément ``tagsDecl`` contient la description des mises en formes utilisées
dans le document. 

Voir plus bas `Mises en forme <#formatting>`__ et `Direction du texte <#text-direction>`__.

Exemple de chapitre de livre (export OE) 

.. code-block:: xml

   <encodingDesc>
   <tagsDecl>
      <rendition xml:id="rendition_list1" scheme="css">list-style-type:upper-roman</rendition>
      <rendition xml:id="rendition_list2" scheme="css">list-style-type:decimal</rendition>
      <rendition xml:id="rendition_list3" scheme="css">list-style-type:lower-alpha</rendition>
      <rendition xml:id="rtl" scheme="css">direction:rtl;</rendition>
   </tagsDecl>
   </encodingDesc>


profileDesc
~~~~~~~~~~~~~~~~~~~~

Langue
^^^^^^

L’élément ``langUsage`` -> ``language`` permet de renseigner la langue du
document.

XPath :

-  Langue : ``/TEI/teiHeader/profileDesc/langUsage/language/@ident``

Recommandations d’usage :

-  La langue doit être exprimée selon la `RFC
   5646 <https://www.rfc-editor.org/info/rfc5646>`__ en utilisant la
   forme "Language-Region". Par exemple :

-  Anglais des Etats-Unis : en-US

-  Anglais du Royaume-Uni : en-GB

Exemple :

.. code-block:: xml

  <profileDesc>
   <langUsage>
    <language ident="fr-FR"/>
   </langUsage>
  </profileDesc>
..

Index : mots clés, géographie, chronologie, thèmes, etc.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’élément ``textClass`` -> ``keywords`` permet de renseigner les mots clés
relatifs au document, et regroupés en différentes typologies.

XPath : ``/TEI/teiHeader/profileDesc/textClass/keywords[@scheme and @xml:lang]/list/item``

XPath :

-  Index simples de mots-clés dans différentes langues ("XX') :  ``/TEI/teiHeader/profileDesc/textClass/keywords[@scheme="keyword" and @xml:lang="XX"]``


-  Autres index : ``/TEI/teiHeader/profileDesc/textClass/keywords[@scheme="XXX"]``.
   L’attribut ``@scheme`` peut prendre les valeurs suivantes selon le type
   d’index :

	*  Géographie : geography

	*  Chronologie : chronology

	*  Thème : subject  

-  Élément de chaque index : relativement aux XPaths précédents, chaque
   entrée d’index est listée dans un élément ``./list/item``

-  Index de personnes citées : ``/TEI/teiHeader/profileDesc/textClass/keywords[@scheme="personcited"]``.
   Chaque entrée d’index est listée dans un élément ``./list/item`` avec :

	*  Prénom : ``./persName/forename``

	*  Nom : ``./persName/surname``

Recommandations d’usage :

-  Pour les mots-clés la langue est au format `ISO
   639-1 <https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1>`__
   ("fr", "en", "de", etc.).

-  Par défaut seules les langues ``fr`` et ``en`` sont activées sur les sites OpenEdition Books, les autres langues sont activables sur demande

-  Les index ``geography``, ``chronology``, ``subject``  s'utilisent avec une liste fermée d'entrées, à définir dans le site OpenEdition Books.


Exemple : 


.. code-block:: xml

   <textClass>
   <keywords scheme="keyword" xml:lang="fr">
      <list >
         <item>humanités numériques</item>
         <item>politique</item>
         <item>science</item>
      </list>
   </keywords>
   <keywords scheme="keyword" xml:lang="en">
      <list>
         <item>digital humanities</item>
         <item>political</item>
         <item>science</item>
      </list>
   </keywords>
   <!-- d'autres langues sont supportées pour les keywords --> 
   <keywords scheme="geography">
      <list>
         <item>Paris</item>
         <item>Texas</item>
      </list>
   </keywords>
   </textClass>




text
----

front
~~~~~

L’élément ``front`` contient le paratexte préalable au texte lui-même :
résumés, éléments sur l’oeuvre commentée, note de l’auteur, dédicaces,
etc.

Résumés
^^^^^^^

XPath :

-  Résumé : ``/TEI/text/front/div[@type="abstract" and @xml:lang]``

Recommandations d’usage :

-  Pour les résumés, la langue est au format `ISO
   639-1 <https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1>`__
   (‘fr’, ‘en’, ‘de’, etc.).

-  Plusieurs paragraphes possibles dans chaque résumé.

-  Ne pas utiliser de note dans les résumés.

Exemple : 

.. code-block:: xml

  <front>
   <div type="abstract" xml:lang="fr">
    <p>Résumé en français, avec <hi rend="italic">des mises en forme</hi>.</p>
   </div>
   <div type="abstract" xml:lang="en">
    <p>English abstract, several paragraphs</p>
    <p>Second paragraph</p>
   </div>
  </front>


Note de l’auteur, note de la rédaction, erratum, remerciements, dédicace
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

XPath :

-  Note de l’auteur : ``/TEI/text/front/note[@type="aut"]``

-  Note de l’éditeur, de la rédaction : ``/TEI/text/front/note[@type="pbl"]``

-  Note du traducteur : ``/TEI/text/front/note[@type="trl"]``

-  Erratum : ``/TEI/text/front/div[@type="correction"]``

-  Remerciements : ``/TEI/text/front/div[@type="ack"]``

-  Dédicace : ``/TEI/text/front/div[@type="dedication"]``

Recommandations d’usage :

-  Plusieurs paragraphes possibles dans chaque élément.

Exemple : 

.. code-block:: xml

  <front>
   <note type="aut">
    <p>Note de l’auteur, premier paragraphe.</p>
    <p>Note de l’auteur, second paragraphe.</p>
   </note>
   <note type="trl">
    <p>Note du traducteur</p>
   </note>
   <note type="pbl">
    <p>Note de l’éditeur, de la rédaction.</p>
   </note>
   <div type="correction">
    <p>Erratum, premier paragraphe.</p>
    <p>Erratum, second paragraphe.</p>
   </div>
   <div type="ack">
    <p>Remerciements.</p>
   </div>
   <div type="dedication">
    <p>Dédicace, premier paragraphe.</p>
    <p>Dédicace, second paragraphe.</p>
   </div>
  </front>
..

Épigraphe
^^^^^^^^^

Placé dans le ``front``, l’épigraphe (``epigraph``) se rapporte au document dans
son ensemble.

XPath : ``/TEI/text/front/epigraph`` qui contient

-  le texte de l’épigraphe : ``./cit/``

-  Référence de la source de l’épigraphe : ``./cit/bibl``

Recommandations d’usage :

-  Si plusieurs paragraphes, utiliser des ``lb`` dans l’émement ``quote``

Exemple : 

.. code-block:: xml

  <front>
   <epigraph>
    <cit>
     <quote>Epigraphe placé dans le front. Porte sur l’ensemble du document.<lb/>
         Second paragraphe d’épigraphe.</quote>
     <bibl>
      <hi rend="italic">Réf bibliographique</hi>, source de l’épigraphe.</bibl>
    </cit>
   </epigraph>
  </front>

body
~~~~

L’élément ``body`` contient tout le corps de texte à l’exclusion des parties
pré- ou post-liminaire.

Structure du texte et intertitres
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

XPath :

-  Sections : ``//div[@type="sectionN"]`` (N compris entre 1 et 6)

-  Interitres : ``//head``

Recommandations d’usage :

-  le texte du document doit être structuré par des sections (balises
   ``div`` avec un attribut ``@type="sectionN"`` où ``"sectionN"`` peut prendre
   toutes les valeurs comprises entre ``"section1"`` et ``"section6"``).

-  les intertitres doivent être indiqués comme premier élément de la
   section dans une balise ``head``.

Exemple : 

.. code-block:: xml

  <!-- [...] --><div type="section1">
   <head>1. ...</head>
   <div type="section2">
    <head>1.1. ...</head>
    <p>...</p>
    <div type="section3">
     <head>1.1.1. ...</head>
     <p>...</p>
    </div>
    <div type="section3">
     <head>1.1.2. ...</head>
     <p>...</p>
    </div>
   </div>
   <div type="section2">
    <head>1.2. ...</head>
    <p>...</p>
   </div>
  </div>
  <div type="section1">
   <head>2. ...</head>
   <p>...</p>
  </div>
  <!-- [...] -->

Notes de bas de page et notes de fin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

XPath :

-  Note de bas de page : ``//note[@place="foot" and @n]``

-  Note de fin : ``//note[@place="end"and @n]``

Recommandations d’usage :

-  Les notes sont insérées dans le texte dans des balises ``note``

-  L’attribut ``@place`` indique le type de note (``foot`` ou ``end``)

-  L’attribut ``@n`` indique le numéro de la note.

-  La note peut contenir des paragraphes, listes, tableaux, images,
   référence bibliographiques, etc.

Exemple : 

.. code-block:: xml

  <p>Texte <note place="foot" n="1">
    <p>Paragraphe</p>
    <list type="unordered">
     <item>Elément de liste 1</item>
     <item>Elément de liste 2</item>
    </list>
   </note>. </p>
  <p>Texte<note place="end" n="a">
    <p>Nulla consequat massa quis enim.</p>
    <list type="unordered">
     <item>Aenean auctor sapien quis nibh tincidunt</item>
     <item>eget varius elit maximus</item>
    </list>
    <p>Phasellus at eros imperdiet, efficitur nisi vitae.</p>
   </note>.
  </p>

.. _formatting:

Mises en forme du texte
^^^^^^^^^^^^^^^^^^^^^^^

XPath :

-  italique : ``//hi[@rend="italic"]``

-  gras : ``//hi[@rend="bold"]``

-  exposant : ``//hi[@rend="sup"]``

-  indice : ``//hi[@rend="sub"]``

-  petite capitale : ``//hi[@rend="small-caps"]``

-  majuscule : ``//hi[@rend="uppercase"]``

-  barré : ``//hi[@rend="strikethrough"]``

-  souligné : ``//hi[@rend="underline"]``


Recommandations d’usage :

-  On utilise l’élément ``hi`` avec l’attribut ``@rend`` qui peut prendre les
   valeurs définies dans les XPath ci-dessus.

-  Les éléments ``hi`` peuvent être imbriqués, comme dans l’exemple plus
   bas.

-  Les valeurs d’attributs peuvent être combinés dans un même
   attribut ``@rend``, séparés par des espaces, comme dans l’exemple plus bas.

Exemple : 

.. code-block:: xml

  <p>Mises en forme de texte simples :
   <hi rend="italic">italique</hi>,
   <hi rend="bold">gras</hi>,
   <hi rend="sup">exposant</hi>,
   <hi rend="sub">indice</hi>,
   <hi rend="small-caps">petite capitale</hi>,
   <hi rend="uppercase">majuscule</hi>,
   <hi rend="strikethrough">barré</hi>,
   <hi rend="underline">souligné</hi>.
  </p>
  <p>Mises en forme de texte imbriquées :
   <hi rend="italic">
     <hi rend="bold">italique et gras</hi>
    </hi>,
   <hi rend="bold">
     <hi rend="small-caps">XX</hi>
     <hi rend="sup">e</hi> siècle</hi>.
  </p>
  <p>Mises en forme combinées :
   <hi rend="bold italic">gras et italique</hi>,
   <hi rend="sup small-caps underline">exposant, petites capitales et souligné</hi>.
  </p>

.. _text-direction:

Direction du texte
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Recommandations d’usage :

- La direction du texte par défaut sur la plateforme OpenEdition Books est de gauche à droite.

- Pour indiquer une direction de texte de droite à gauche (arabe, hébreu notamment), il faut ajouter un attribut ``@rendition`` qui fait référence à un style défini dans l’élément ``tagsDecl`` du ``header`` avec la propritété css ``direction:rtl;``.

- L'attribut rendition pour spécifier la direction du texte peut être appliquée aux éléments suivants : ``p``, ``title``, ``quote``, ``head``, ``item``, ``bibl``, ``cell``, ``speaker``, ``l``, ``lg``, ``stage``, ``label``, ``gloss``.

Exemple : 

.. code-block:: xml

   <encodingDesc>
     <tagsDecl>
       <rendition xml:id="rtl" scheme="css">direction:rtl;</rendition>
     </tagsDecl>
   </encodingDesc>

   [...]

   <p rendition="#rtl">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل</p>


Listes
^^^^^^

XPath :

-  Éléments de liste non-ordonnée : ``//list[@type="unordered"]/item``

-  Éléments de liste ordonnée : ``//list[@type="ordered"]/item``

Recommandations d’usage :

-  Possibilité d’imbriquer des éléments de listes ordonnées ou non
   ordonnées

-  Possibilité de continuer la numérotation d'une liste précédente avec l'attribut ``@prev``

-  Possibilité de définir un type de numérotation avec l’attribut
   ``@rendition`` sur l’élément ``list``

-  l’attribut ``@rendition`` fait référence à un style défini dans l’élément
   ``tagsDecl`` du ``header``.

-  Valeurs autorisées de l’attribut ``@rendition`` pour les listes non
   ordonnées :

	*  ``list-style-type:disc``

	*  ``list-style-type:square``

	*  ``list-style-type:circle``

-  Valeurs autorisées de l’attribut ``@rendition`` pour les listes ordonnées
   :

	*  ``list-style-type:decimal``

	*  ``list-style-type:lower-roman``

	*  ``list-style-type:upper-roman``

	*  ``list-style-type:lower-alpha``

	*  ``list-style-type:upper-alpha``

-  Les valeurs d’attributs peuvent être combinés dans un même
   attribut ``@rend``, séparés par des espaces, comme dans l’exemple plus bas.


Exemple : 

.. code-block:: xml

  <teiHeader> 
  [...]
  <encodingDesc>
    <tagsDecl>
     <rendition xml:id="rendition_list1"
         scheme="css">list-style-type:upper-roman</rendition>
     <rendition xml:id="rendition_list2"
         scheme="css">list-style-type:decimal</rendition>
     <rendition xml:id="rendition_list3"
         scheme="css">list-style-type:lower-alpha</rendition>
     <rendition xml:id="rendition_list4"
         scheme="css">list-style-type:upper-alpha</rendition>
     <rendition xml:id="rendition_list5"
         scheme="css">list-style-type:lower-roman</rendition>
     <rendition xml:id="rendition_list6"
         scheme="css">list-style-type:disc</rendition>
     <rendition xml:id="rendition_list7"
         scheme="css">list-style-type:circle</rendition>
     <rendition xml:id="rendition_list8"
         scheme="css">list-style-type:square</rendition>
    </tagsDecl>
   </encodingDesc>
   [...]
  </teiHeader>
  <text>
   <body>
    <p>Une liste avec une numérotation décimale</p>
    <list type="ordered" xml:id="list01"
        rendition="#rendition_list2">
     <item>premièrement</item>
     <item>deuxièmement</item>
     <item>troisièmement</item>
    </list>
    <p>suivi d'une autre liste avec numérotation continue</p>
    <list type="ordered" xml:id="list02"
        prev="#list01" rendition="#rendition_list2">
     <item>quatrièmement</item>
     <item>cinquièmement</item>
    </list>
    <p>Puis une autre liste avec le style de numérotation upper-roman</p>
    <list type="ordered" xml:id="list03"
        rendition="#rendition_list1">
     <item>en 1</item>
     <item>en 2</item>
    </list>
    <p>Exemple d'imbrication de liste ordonnées</p>
    <list type="ordered" xml:id="list04"
        rendition="#rendition_list4">
     <item>Item A</item>
     <item>Item B</item>
     <item>Item C
     <list type="ordered" xml:id="list05"
          rendition="#rendition_list3">
       <item>sous item a</item>
       <item>sous item b</item>
      </list>
     </item>
    </list>
    <p>Pour les listes non ordonnées on a le choix entre 3 styles de puces : disc, square,
  circle</p>
    <list type="unordered" xml:id="list06"
        rendition="#rendition_list6">
     <item>petit 1</item>
     <item>petit 2</item>
     <item>petit 3</item>
    </list>
    <list type="unordered" xml:id="list07"
        rendition="#rendition_list7">
     <item>Thé sans sucre et sans lait </item>
     <item>Un jus d'ananas</item>
     <item>Un yaourt</item>
     <item>Trois biscuits de seigle avec
     <list type="unordered" xml:id="list08"
          rendition="#rendition_list8">
       <item>Carottes râpées</item>
       <item>Côtelettes d'agneau</item>
       <item>Courgettes</item>
       <item>Chèvre frais </item>
       <item>Coings</item>
      </list>
     </item>
    </list>
   </body>
  </text>
..

Citations
^^^^^^^^^

XPath :

-  Bloc de citation : ``//*[not(name()=‘p’) and not(name()=‘item’)]/cit``

-  Paragraphe dans la citation (XPath relatif à l’élément ``cit`` : ``./quote``

-  Paragraphe dans la citation (avec mise en forme différente / XPath
   relatif) : ``./quote[@type="quotation2"]``

-  Citation traduite ou dans une autre langue (XPath relatif) :
   ``./quote[@xml:lang and @type="trl"]``

-  Référence bibliographique de la citation (XPath relatif) : ``./bibl``

-  Citation inline : ``//*[name()=‘p’ or name()=‘item’]/cit/quote``

Recommandations d’usage :

-  Une citation peut être composée de plusieurs paragraphe (``quote``).

-  La forme peut varier en utiliant l’attribut ``@type="quotation2"`` sur
   ``quote``.

-  La citation peut être sourcée avec l’élément ``bibl`` à la fin de
   l’élément ``cit``.

-  On peut indiquer la langue de l’élement ``quote`` pour la traduction de
   la citation qui précède ou pour indiquer la langue de la citation. La
   langue doit être renseignée au format `ISO
   639-1 <https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1>`__
   (‘fr’, ‘en’, ‘de’, etc.).

-  Il est possible d’imbriquer des éléments ``cit`` dans le cas d’un
   citation citée dans une autre citation (voir exemple plus bas).

-  La citation peut être "inline". L’élément ``cit`` est alors placé au
   milieu du texte du paragraphe.

Exemple : 

.. code-block:: xml

  <div type="section2">
   <head>Citations simples avec plusieurs paragraphes</head>
   <p>Un paragraphe de texte Normal avant la citation.</p>
   <cit xml:id="cit1">
    <quote>Une citation en plusieurs paragraphe.</quote>
    <quote>Second paragraphe.</quote>
    <bibl>Référence biblio de ma citation</bibl>
   </cit>
   <p>Un autre paragraphe Normal, suivi d’une autre citation avec un style différent (pas
  de distinction sémantique mais une mise en forme différente des citations classiques)
  :</p>
   <cit xml:id="cit2">
    <quote type="quotation2">Une citation avec mise en forme différente.</quote>
    <bibl>Référence biblio de ma deuxième citation</bibl>
   </cit>
  </div>
  <div type="section2">
   <head>Traductions</head>
   <p>Une citation suivie de sa traduction :</p>
   <cit xml:id="cit3">
    <quote>Une citation avec le style citation</quote>
    <quote>La citation peut avoir plusieurs paragraphes.</quote>
    <quote xml:lang="en" type="trl">La même citation traduite dans une autre langue.</quote>
    <quote xml:lang="en" type="trl">La citation traduite peut avoir plusieurs paragraphe elle aussi.</quote>
    <bibl>Référence biblio de la citation</bibl>
   </cit>
   <p>Une citation dans une langue différente de l'article, ici en espagnol.</p>
   <cit xml:id="cit4">
    <quote xml:lang="es">De gustibus et coloribus non disputandum</quote>
   </cit>
  </div>
  <div type="section2">
   <head>Citations consécutives (sourcées ou non)</head>
   <p>Ici il s’agit de deux citation consécutives, mais distinctes. Dans ce cas, on utilise
  2 éléments <cit></p>
   <cit xml:id="cit5">
    <quote>Txt 1ère citation</quote>
   </cit>
   <cit xml:id="cit6">
    <quote>Txt 2e citation</quote>
    <bibl>Ref biblio 2e citation</bibl>
   </cit>
  </div>
  <div type="section2">
   <head>Citation inline</head>
   <p>Un paragraĥe peut contenir du texte avec une citation inline.</p>
   <p>Comme le disait Jean, <cit xml:id="cit7">
     <quote>"le renard... (La Fontaine)"</quote>
    </cit>, blah.</p>
   <p>Même chose dans un liste.</p>
   <list rendition="rendition_list7"
       type="unordered">
    <item>Comme le disait Jean, <cit xml:id="cit8">
      <quote>"le renard... (La Fontaine)"</quote>
     </cit>, blah.</item>
   </list>
   <p>texte</p>
  </div>
  <div type="section2">
   <head>Citations imbriquées</head>
   <cit xml:id="cit9">
    <quote>Ma citation principale qui cite une citation</quote>
    <cit>
     <quote>Ici la citation dans la citation</quote>
     <quote>qui peut être composée de plusieurs paragraphes</quote>
    </cit>
    <quote>On reprend la suite du texte de la citation principale.</quote>
    <bibl>Ref biblio de la citation principale</bibl>
   </cit>
  </div>
..


Paragraphe de suite et séparateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

XPath :

-  Paragraphe de suite : ``//p[@rend="consecutive"]``

-  Séparateur : ``//p[@rend="break"]``

Recommandations d’usage :

-  Le paragraphe de suite est généralement utilisé pour le paragraphe
   qui suit une citation, si celui-ci est la suite du paragraphe qui
   précède la citation

-  Le séparateur permet de marquer une séparation graphique entre deux
   paragraphes.

Exemple : 

.. code-block:: xml

  <p>Début du paragraphe</p>
  <cit>
   <quote>Citation</quote>
  </cit>
  <p rend="consecutive">suite du paragraphe précédant la citation.</p>
  <p rend="break">* * *</p>
  <p>Paragraphe suivant.</p>
..


Références bibliographiques
^^^^^^^^^^^^^^^^^^^^^^^^^^^

XPath :

-  Référence bibliographique: ``//bibl``

Recommandations d’usage :

-  Les références bibliographiques doivent être encodées avec un élement
   ``bibl``.

-  L’élement ``bibl`` peut être utilisé :

	*  comme un paragraphe dans le corps du texte (``body``), dans les annexes, dans les notes de bas de page et notes de fin.

	*  en citation inline à l’intérieur d’un paragraphe ou d’une liste, dans le corps du texte (``body``), dans les annexes, dans les notes de bas de page et notes de fin.

	*  dans un élément ``cit`` pour indiquer la source d’une citation (y compris dans l' élément ``epigraph``)

	*  après un élément ``sp`` ou ``lg`` pour indiquer la source d’une citation de Théâtre ou de Poésie.

	*  dans l’élément ``back`` pour renseigner la bibliographie du document (voir section `Bibliographie <#bibliography>`__)

Exemple : 

.. code-block:: xml

   <text>
    <front>
     <epigraph>
      <cit>
       <quote>Nulla tincidunt metus sit amet diam porta, in malesuada lectus mollis. </quote>
       <bibl>Référence épigraphe Front</bibl>
      </cit>
     </epigraph>
    </front>
    <body>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <note xml:id="ftn1" n="1" place="foot">
       <bibl>Référence note 1</bibl>
      </note>. Curabitur egestas rutrum purus ac varius. Maecenas blandit eget orci non placerat.
   Sed hendrerit justo et augue dapibus, at hendrerit tellus dictum. Nam auctor accumsan
   ante et mattis (<bibl>référence inline 1</bibl>). </p>
     <p>Donec leo purus, cursus vel nulla at, pulvinar eleifend erat. Praesent consectetur
   purus elementum, consequat nisl vitae, tincidunt elit. Proin<note xml:id="ftn2" n="2" place="foot">
       <p>Note 2 avec une <bibl>référence inline note 2</bibl>
       </p>
      </note> non sapien neque. Nam suscipit interdum leo quis dapibus. Cras sollicitudin elit nulla,
   non placerat ipsum molestie id.</p>
     <bibl>Référence dans le texte 1.</bibl>
     <cit>
      <quote>Vestibulum vulputate, diam tincidunt laoreet aliquet, mi odio egestas ex, sit amet
   scelerisque metus nulla vitae massa.</quote>
      <bibl>Référence citation 1</bibl>
     </cit>
     <sp>
      <p>Nulla tincidunt metus sit amet diam porta, in malesuada lectus mollis.</p>
     </sp>
     <bibl>Référence Theatre 1</bibl>
     <sp>
      <l>Nunc eget ligula vestibulum, ultrices felis eu, tincidunt ante. Phasellus at vehicula
   enim></l>
      <l>Vivamus sed posuere nulla. Vivamus id purus lorem. Praesent iaculis cursus tortor
   at porttitor.</l>
     </sp>
     <bibl>Référence Theatre 2</bibl>
     <lg>
      <l>Ut nisi libero, fermentum nec suscipit a,</l>
      <l>ultricies sit amet odio. Nulla non sem sed dui semper maximus.</l>
      <l>Aliquam hendrerit odio arcu.</l>
      <l>Quisque vitae egestas justo. Nulla non iaculis massa.</l>
     </lg>
     <lg>
      <l>Nulla posuere libero mauris,</l>
      <l>non dignissim sem porttitor lacinia.</l>
      <l>In tempus arcu sed mollis varius. Etiam a felis risus.</l>
      <l>Vestibulum vestibulum metus vitae lacus imperdiet vehicula.</l>
     </lg>
     <bibl>Référence poésie 1</bibl>
     <div type="section1">
      <head>Titre 1</head>
      <epigraph>
       <cit>
        <quote>Suspendisse sollicitudin odio eu felis interdum cursus. Aenean a justo vestibulum,
   sollicitudin ex nec, commodo nibh. Duis at vulputate ligula, a dictum arcu.</quote>
        <bibl>Référence épigraphe texte</bibl>
       </cit>
      </epigraph>
      <p>Nulla tristique placerat dapibus. Proin quis est tincidunt, volutpat elit et, condimentum
   nisl.</p>
      <list type="unordered" xml:id="list01"
          rendition="#rendition_list">
       <item>petit 1</item>
       <item>
        <bibl>Référence dans une liste</bibl>
       </item>
       <item>Et une <bibl>référence inline texte</bibl>dans une liste</item>
      </list>
     </div>
    </body>
    <back>
     <div type="bibliography"
         xml:id="bibliography">
      <bibl>Référence biblio 1</bibl>
      <bibl>Référence biblio 2</bibl>
      <bibl>Référence biblio 3</bibl>
     </div>
     <div type="appendix" xml:id="appendix">
      <p>Suspendisse felis metus, venenatis ac risus quis, volutpat rutrum ex. Sed rutrum neque
   ac suscipit pulvinar. </p>
      <bibl>Référence dans une annexe</bibl>
      <p>Nam maximus dolor nec justo venenatis, sit amet pretium turpis cursus. Vestibulum
   dictum id lectus id porttitor. Curabitur at rutrum diam, et mollis nisi. Sed ut ultricies
   orci. Sed malesuada in turpis nec vestibulum. Aenean tortor dolor, egestas sit amet
   maximus a, faucibus ut mi.</p>
     </div>
    </back>
   </text>



Liens hypertexte
^^^^^^^^^^^^^^^^

XPath :

-  Liens hypertexte: ``//ref``

Recommandations d’usage :

-  Utiliser l’élément ``ref`` avec l’attribut ``@target`` pour encoder un lien
   externe (http) ou interne au document (vers un attribut ``@xml:id`` qui
   peut être placé sur tous les éléments enfants de ``body`` et ``back``).

-  On peut utiliser l’élément ``anchor`` avec l’attribut ``@xml:id`` pour définir
   une ancre vers laquelle pointer.

Exemple : 

.. code-block:: xml

  <cit xml:id="ref_cit1">
   <quote>Citation</quote>
  </cit>
  <cit>
   <quote>Citation <anchor xml:id="anchor_ref2"/>lorem ispum</quote>
  </cit>
  <p>Ici <ref target="#ref_cit1">un lien vers la citation</ref> ; là <ref target="http://www.openedition.org">un lien vers un site web</ref> et un <ref target="#anchor_ref2">lien vers une ancre</ref>.</p>

Code
^^^^

XPath :

-  Code : ``//p/code[@lang]``

Recommandations d’usage :

-  L’élément ``code`` permet d’encoder du code informatique. On utilise
   l’attribut optionnel ``@lang`` pour préciser le langage.

-  L’élément ``code`` doit toujours être placé dans une élement ``p``. Par
   défaut il sera considéré comme un bloc de code.

-  Pour insérer le code dans le texte d’un paragraphe, il faut utiliser
   l’attribut ``@rend="inline"``.

-  À l’intérieur de l’élément ``code``, on peut utiliser un ``"CDATA"`` ou
   encoder les caractères XML : "<", ">", "&".

Exemple : 

.. code-block:: xml

  <!-- Code avec CDATA --><p>
   <code lang="xml"><![CDATA[<ref target="https://www.openedition.org/​">OpenEdition : portail de ressources
  électroniques en sciences humaines et sociales</ref>]]>
   </code>
  </p>
  <!-- un exemple sans CDATA -->
  <p>
   <code lang="php"> if($a == $b){
     echo "$a == $b";
     }
   </code>
  </p>
  <!-- code inline -->
  <p>En php, <code rend="inline" lang="php">echo</code> affiche une chaîne de caractères.</p>
..

D’autres exemples avec encodage d’élement XML dans le fichier d’exemple "Lorem Ipsum".

Encadré
^^^^^^^

XPath :

-  Encadré : ``//floatingText/body``

Recommandations d’usage :

-  L’élément ``floatingText`` représente un encadré qui peut contenir
   n’importe quel contenu TEI (des sections, des citations, des figures,
   etc.)

-  ``floatingText`` doit obligatoirement contenir un élémement ``body``.

Exemple : 

.. code-block:: xml

  <floatingText>
   <body>
    <div type="section1"
        xml:id="floatingText1div1">
     <head>Titre principal de l'encadré</head>
     <div type="section2"
         xml:id="floatingText1div2">
      <head>Un bloc citation dans l’encadré</head>
      <p>Un paragraphe.</p>
      <cit xml:id="floatingText1cit1">
       <quote>« En tant que randonneur, l’expérience de la marche, ça a beaucoup marqué les
             choses, notre façon de percevoir le paysage. Il y a eu la question du sol qui change.
  Au départ, il est très mou, très feutré. Puis on change de côte, et là il se fait bien
  plus dur avec les racines des arbres ».</quote>
       <quote>« En termes de sons, ça semblait bien plus dilué ».</quote>
       <quote>« On voit différentes hostilités selon les côtes, la chaleur, le
             glissement ».</quote>
       <bibl>Relevés de verbatims extraits du carnet de terrain, Fonticelli, 2020.</bibl>
      </cit>
     </div>
     <div type="section2"
         xml:id="floatingText1div3">
      <head>Un bloc figure dans l’encadré</head>
      <figure xml:id="floatingText1figure1">
       <head>Figure 3 : Les attentes des étudiant(e)s au début du module « Atlas ».</head>
       <graphic url="images/Image3.png"/>
      </figure>
     </div>
     <div type="section2"
         xml:id="floatingText1div6">
      <head>Des questions et des réponses </head>
      <sp rend="question">
       <p xml:id="floatingText1p2">Bonjour, vous allez bien ?</p>
      </sp>
      <sp rend="answer">
       <p xml:id="floatingText1p3">Bien, merci.</p>
      </sp>
     </div>
    </div>
   </body>
  </floatingText>

Figure
^^^^^^

XPath :

-  Figure bloc : ``//figure[not(@rend="inline")]``

-  Figure inline : ``//figure[@rend="inline"]``

-  Titre de figure : ``//figure/head``

-  Légende de figure : ``//figure/p[@rend="caption"]``

-  Crédits de figure : ``//figure/p[@rend="credits"]``


Recommandations d’usage :

-  On utilise figure sans attribut ``@rend="inline"`` pour définir un bloc
   d’illustation

-  Ce bloc peut contentir des métadonnées (titre, légende, crédits) et
   l’illustation elle-même (image, tableau, formule).

-  Pas de contrainte technique dans l’ordre des métadonnées et de
   l’illustration.

-  L’élément figure avec attribut ``@rend="inline"`` définit un conteneur
   pour une image ou une formule (pas un tableau) inline, à l’intérieur
   d’un paragraphe.

-  Les figure inline ne peuvent pas contenir de métadonnées.

-  Un élement figure peut contenir plusieurs illustrations de même
   nature, par exemple une planche de plusieurs images, avec des
   métadonnées communes.

Exemple :

.. code-block:: xml

  <div type="section2">
   <head>Un bloc figure avec métadonnées</head>
   <p>Un paragraphe</p>
   <figure>
    <head>Le titre illustration </head>
    <graphic url="images/image3.jpg"/>
    <figDesc>La description d'une image dans un bloc Figure</figDesc>
    <p rend="caption">La légende.</p>
    <p rend="caption">La suite de la légende.</p>
    <p rend="credits">Les crédits</p>
   </figure>
  </div>
  <div type="section2">
   <head>Images inline</head>
   <p>Un paragraphe Normal avec une image <figure rend="inline">
     <graphic url="images/image5.jpg"/>
    </figure>dans le paragraphe.</p>
  </div>
..

Image
^^^^^

XPath :

-  Image : ``//figure/graphic``

-  Fichier image : ``//figure/graphic@src``

-  Description de l'image : ``//figure/figDesc``

Recommandations d’usage :

-  L’élément ``graphic`` doit toujours être utilisé dans un élement ``figure``
   (voir plus haut)

-  L’attribut ``@src`` de l’élément ``graphic`` doit contenir le chemin de
   l’image relativement au fichier TEI.

-  L’élément ``figDesc`` contient une description textuel de l’image
   (différent de la légende)‌.‌ Son usage est vivement encouragé dans le
   but d’améliorer l’accessibilité (déficience visuelle).

-  L’élément ``figDesc`` doit suivre directement l’élement ``graphic`` auquel il
   se rapporte.

Exemple : 

.. code-block:: xml

  <figure>
   <head>Le titre de la planche </head>
   <graphic url="images/image4-1.jpg"/>
   <figDesc>La description de l'image qui précède</figDesc>
   <graphic url="images/image4-2.jpg"/>
   <figDesc>La description de l'image qui précède</figDesc>
   <graphic url="images/image4-3.jpg"/>
   <figDesc>La description de l'image qui précède</figDesc>
   <graphic url="images/image4-4.jpg"/>
   <figDesc>La description de l'image qui précède</figDesc>
   <p rend="caption">La légende de la planche</p>
   <p rend="credits">Les crédits</p>
  </figure>

Tableau
^^^^^^^

XPath :

-  Tableau : ``//table``

-  Ligne (normale) : ``//table/row[not(@role="label")]``

-  Ligne d’entête : ``//table/row[@role="label"]``

-  Cellule (normale) : ``//table/row/cell[not(@role="label")]``

-  Cellule d’entête (en première colonne) : ``//table/row/cell[@role="label"]``

-  Mise en forme des cellules : ``//table/row/cell/@rendition``

-  Fusion de lignes : ``//table/row/cell/@rows``

-  Fusion de colonnes : ``//table/row/cell/@cols``

-  Image alternative : ``//figure/graphic[preceding-sibling::table]``

Recommandations d’usage :

-  L’élément ``table`` doit toujours être utilisé dans un élement ``figure``
   (voir plus haut)

-  On utilise l’attribut ``@role="label"`` :

	*  sur l’élément ``row`` pour définir un ligne d’entête (titre de colonne)

	*  sur l’élément ``cell`` pour définir une cellule titre (titre de ligne)

-  On utilise un élement ``graphic`` à la suite de l’élément ``table`` pour
   définir une image alternative du tabeau. Elle sera utilisée pour
   l’affichage du tableau dans les environnements qui ne permettent pas
   un affichage correct de celui-ci.

-  L’attribut ``@rendition`` permet la mise en forme des cellules (bordures, alignement, couleur de fond de cellule).

Exemple : 

.. code-block:: xml

  <teiHeader>
   <encodingDesc>
    <tagsDecl>
     <rendition scheme="css" xml:id="Cell">border-top:2.25pt solid #000000; border-right:0.5pt
         solid #ffffff; border-bottom:1pt solid #93c571; border-left:1pt solid #93c571; </rendition>
    </tagsDecl>
   </encodingDesc>
  </teiHeader>
  <text>
   <body>
    <figure xml:id="figure9">
     <head>Titre du tableau</head>
     <table>
      <row role="label">
       <cell rendition="#Cell">Entête A</cell>
       <cell rendition="#Cell">Entête B</cell>
       <cell rendition="#Cell">Entête C</cell>
      </row>
      <row>
       <cell rendition="#Cell" role="label">A1 : titre ligne</cell>
       <cell rendition="#Cell">B1</cell>
       <cell rendition="#Cell">C1</cell>
      </row>
      <row>
       <cell rendition="#Cell" role="label">A2 : titre ligne</cell>
       <cell rendition="#Cell" cols="2">B2, C2</cell>
      </row>
     </table>
     <graphic url="images/image7.png"/>
  <!-- Image alternative du tableau -->
     <p rend="caption">Légende du tableau</p>
     <p rend="credits">Crédits du tableau</p>
    </figure>
   </body>
  </text>


Formule
^^^^^^^

.. note::

   L'affichage des formules est en cours de développement pour la plateforme OpenEdition Books

..

XPath :

-  Formule bloc (LaTeX) : ``//figure[not(@rend="inline")]/formula[@notation="latex"]``

-  Formule inline (LaTeX) : ``//figure[@rend="inline"]/formula[@notation="latex"]``

-  Image alternative : ``//figure/graphic[preceding-sibling::formula]``

Recommandations d’usage :

-  le contenu de l’élément ``formula`` peut être inclus dans un CDATA si
   besoin. Sinon les caractères spéciaux XML ("<", ">"; "&") doivent
   être encodés.

-  On utilise un élement ``graphic`` à la suite de l’élément ``formula`` pour
   définir une image alternative de la formule. Elle sera utilisée pour
   l’affichage de la formule dans les environnements qui ne permettent
   pas un affichage correct de celle-ci

-  Les formules LaTeX de type "bloc" doivent être insérées entre "[" et
   "]"

-  Les formules LaTeX de type "inine" doivent être insérées entre "(" et
   ")"

Exemple : 

.. code-block:: xml

  <div type="section1" xml:id="div7">
   <head>Formule inline</head>
   <p>Une formule mathématique inline <figure rend="inline">
     <formula notation="latex">\(\frac{{{\partial ^2}v}}{{\partial {z^2}}} = 0\)</formula>
    </figure> suivi de mon texte.</p>
  </div>
  <div type="section1">
   <head>Formule block avec métadonnées et image alternatice</head>
   <figure>
    <head>Titre formule </head>
    <formula notation="latex">\[\displaystyle \frac {d^2x^a}{ds^2}+\Gamma_{bc}^{a} \frac {dx^b}{ds}\frac {dx^c}{ds}=0\]</formula>
    <graphic url="images/image2.png"/>
  <!-- Image alternative de la formule -->
    <p rend="caption">Légende Formule</p>
   </figure>
  </div>


Entretien
^^^^^^^^^

XPath :

-  Bloc question : ``//sp[@rend="question"]``

-  Bloc réponse : ``//sp[@rend="answer"]``

-  Locuteur dans un paragraphe distinct : ``//sp/speaker``

-  Locuteur inline : ``//sp/p/name[@type="speaker"]``

Recommandations d’usage :

-  On encode les blocs "question" et "réponse" avec des élements ``sp`` qui
   contiennent un ou plusieurs paragraphes.

-  Le locuteur peut être mentionné dans un paragraphe distinct avec
   l’élement ``speaker`` ou en début de paragraphe avec l’élement ``name``.

Exemple : 

.. code-block:: xml

  <p>Lorem ipsum</p>
  <sp rend="question">
   <p>Première question</p>
  </sp>
  <sp rend="answer">
   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
     imperdiet scelerisque diam, lobortis pulvinar nunc gravida sit amet.</p>
   <p>Vestibulum vulputate, diam tincidunt laoreet aliquet, mi odio egestas
     ex, sit amet scelerisque metus nulla vitae massa.</p>
  </sp>
  <sp rend="question">
   <p>
    <name type="speaker">Personne 1</name> : Question 2</p>
  </sp>
  <sp rend="answer">
   <speaker>Locuteur 1</speaker>
   <p>Nulla tincidunt metus sit amet diam porta, in malesuada lectus
     mollis. Sed sed consectetur ex.</p>
  </sp>
  <sp rend="answer">
   <speaker>Locuteur 2</speaker>
   <p>Nunc eget ligula vestibulum, ultrices felis eu, tincidunt ante.
     Phasellus at vehicula enim. In nunc justo, malesuada quis auctor rutrum,
     venenatis nec dolor.</p>
  </sp>
  <sp rend="question">
   <p>Question 3</p>
  </sp>
  <sp rend="answer">
   <p>
    <name type="speaker">Locuteur 1</name> : Ut nisi libero, fermentum
     nec suscipit a, ultricies sit amet odio. Nulla non sem sed dui semper maximus.
     Aliquam hendrerit odio arcu. Quisque vitae egestas justo. Nulla non iaculis
     massa.</p>
  </sp>
  <p>Vestibulum vulputate</p>


Théâtre
^^^^^^^

XPath :

-  Bloc réplique : ``//sp``

-  Locuteur dans un paragraphe distinct : ``//sp/speaker``

-  Locuteur inline : ``//sp/p/name[@type="speaker"]``

-  Réplique versifiée : ``//sp/l``

-  Numéro de vers : ``//sp/l@n et //sp/l/num``

-  Didascalie : ``//stage``

-  Source : ``//bibl[preceding-sibling::sp]``

Recommandations d’usage :

-  Le locuteur peut être mentionné dans un paragraphe distinct avec
   l’élement ``speaker`` ou en début de paragraphe avec l’élement ``name``.

-  Des didascalies peuvent être encodées avec l’élement ``stage``

-  Dans le cas de répliques versifiées, on utilise l’élément ``l`` dans ``sp``
   avec la possiblité de numéroter les vers (voir exemple plus bas).

-  La source (optionnelle) doit être mentionnée dans un élement ``bibl``
   après l’élément ``sp`` le cas échéant.

Exemple :

.. code-block:: xml

  <p>Exemple simple</p>
  <sp>
   <speaker>Locuteur 1</speaker>
   <p>Replique. Ut nisi libero, fermentum nec suscipit a, ultricies sit
     amet odio. Nulla non sem sed dui semper maximus. Aliquam hendrerit odio arcu.
     Quisque vitae egestas justo. Nulla non iaculis massa. </p>
  </sp>
  <stage>Didascalie</stage>
  <sp>
   <p>
    <name type="speaker">Locuteur 2</name> : réplique.</p>
  </sp>
  <sp>
   <p>Nulla non iaculis massa. Nulla posuere libero mauris, non dignissim
     sem porttitor lacinia. In tempus arcu sed mollis varius. Etiam a felis risus.
     Vestibulum vestibulum metus vitae lacus imperdiet vehicula.</p>
  </sp>
  <sp>
   <p>
    <name type="speaker">Locuteur 1</name>— <stage>(didascalie
       inline)</stage> Vestibulum vulputate, diam tincidunt laoreet aliquet, mi
     odio egestas ex, sit amet scelerisque metus nulla vitae massa.</p>
  </sp>
  <p>Exemple avec ligne versifiée avec une source</p>
  <sp>
   <speaker>Locuteur 3</speaker>
   <l n="1">
    <num>1</num> Nunc eget ligula vestibulum, ultrices felis eu, tincidunt
     ante.</l>
  </sp>
  <sp>
   <speaker>Locuteur 4</speaker>
   <l n="2">
    <num>2</num> In nunc justo, malesuada quis auctor rutrum,</l>
  </sp>
  <bibl>Référence de l'oeuvre</bibl>
  <p>Exemple avec des répliques versifiées et source</p>
  <sp>
    <l>Dis-moi donc, je te prie, une seconde fois </l>
    <l>Ce qui te fait juger qu'il approuve mon choix. </l>
  </sp>
  <bibl>(<hi rend="italic">Le Cid, </hi>Chimène vv.7-8)</bibl>
..

Poésie
^^^^^^

XPath :

-  Strophe : ``//lg``

-  Vers : ``//lg/l``

-  Numéro de vers : ``//lg/l@n`` et ``//lg/l/num``

-  Césure : ``//lg/l/caesura``

-  Source : ``//bibl[preceding-sibling::lg]``

Recommandations d’usage :

-  Voir exemple ci-dessous qui contient tous les cas d’usages

-  La source (optionnelle) doit être mentionnée dans un élement ``bibl``
   après l’élément lg le cas échéant.

Exemple : 

.. code-block:: xml

  <div type="section2">
   <head>Exemple simple</head>
   <lg>
    <l>Les amoureux fervents et les savants austères</l>
    <l>Aiment également, dans leur mûre saison,</l>
    <l>Les chats puissants et doux, orgueil de la maison,</l>
    <l>Qui comme eux sont frileux et comme eux sédentaires.</l>
   </lg>
  </div>
  <div type="section2">
   <head>Exemple avec numérotation des vers</head>
   <lg>
    <l n="1">
     <num>(1)</num> Ils prennent en songeant les nobles attitudes</l>
    <l>Des grands sphinx allongés au fond des solitudes</l>
    <l>Qui semblent s'endormir dans un rêve sans fin ;</l>
   </lg>
   <lg>
    <l n="4">
     <num>(4)</num> Leurs reins féconds sont pleins d'étincelles magiques,</l>
    <l>Et des parcelles d'or, ainsi qu'un sable fin,</l>
    <l>Etoilent vaguement leurs prunelles mystiques.</l>
   </lg>
  </div>
  <div type="section2">
   <head>Avec césure et source</head>
   <lg>
    <l>Souvent sur la montagne, <caesura/>à l'ombre du vieux chêne,</l>
    <l>Au coucher du soleil, <caesura/>tristement je m'assieds ;</l>
   </lg>
   <bibl>Alphonse de Lamartine, <hi rend="italic">L'isolement</hi>
   </bibl>
  </div>
..

Exemples de linguistique
^^^^^^^^^^^^^^^^^^^^^^^^

XPath :

-  Exemple : ``//cit[@type="linguistic"]/quote``

-  Titre (Label) de l’exemple : ``//cit[@type="linguistic"]/quote/label``

-  Numéro d’exemple : ``//cit[@type="linguistic"]/quote/label/num || //cit[@type="linguistic"]/quote/num``

-  Langue : ``//lang``

-  Lignes : ``//cit[@type="linguistic"]/quote/quote[@type="example"]``

-  Traduction : ``//cit[@type="linguistic"]/quote/quote[@type="trl"]``

-  Glose : ``//cit[@type="linguistic"]/quote/gloss``

-  Segments : ``//seg``

-  Référence bibliographique : ``//cit[@type="linguistic"]/quote/bibl``

-  Référence bibliographique inline: ``//cit[@type="linguistic"]/quote/quote/bibl[@rend="inline"]``

Recommandations d’usage :

-  l'exemple de linguistique doit être contentu dans un élément ``cit[@type=liguistic"]/quote``

-  Les lignes d’exemples et les gloses doivent être segmentées avec un ou des éléments ``seg``

-  Les numéros d'exemples ``num`` peuvent être utilisé dans le titre de l'exemple (dans ce cas il faut l’encoder ``//cit[@type="linguistic"]/quote/label/num``) ou en début de colonne, avant l'élément ``quote[@type="example"]``

-  L’élément ``lang`` peut être utilisé dans les titres d’exemples, les
   lignes d’exemples ou les gloses

Exemple simple avec label: 

.. code-block:: xml

  <cit type="linguistic" xml:id="cit03">
    <quote>
      <label><num>(1)</num> <lang>French</lang></label>
      <quote type="example">
        <seg>a1</seg>
        <seg>a2</seg>
      </quote>
      <quote type="example">
        <seg>b1</seg>
        <seg>b2</seg>
      </quote>
      <gloss>
        <seg>c1</seg>
        <seg>c2</seg>
      </gloss>
    </quote>
  </cit>
..

Exemple complet avec glose, traduction, numérotations, références bibliographiques : 

.. code-block:: xml

  <cit type="linguistic" xml:id="cit04">
    <quote n="7">
      <label><num>(7)</num> <lang>French</lang> [Contexte : Lorem Ipsum]</label>
      <num>a.</num>
      <quote type="example">
        <seg>M</seg>
        <seg>d</seg>
        <seg>J</seg>
        <seg>a</seg>
        <seg>w</seg>
        <seg>J</seg>
      </quote>
      <gloss>
        <seg>1</seg>
        <seg>d</seg>
        <seg>J</seg>
        <seg>A</seg>
        <seg>v</seg>
        <seg>J</seg>
      </gloss>
      <quote type="trl">‘Lorem ipsum dolor sit amet’<bibl rend="inline">reference</bibl></quote>
      <num>b.</num>
      <quote type="example">
        <seg>E</seg>
        <seg>J</seg>
        <seg>a</seg>
        <seg>w</seg>
        <seg>J</seg>
      </quote>
      <gloss>
        <seg>E</seg>
        <seg>q</seg>
        <seg>J</seg>
        <seg>A</seg>
        <seg>v</seg>
        <seg>J</seg>
      </gloss>
      <quote type="trl">‘consectetur adipiscing elit’</quote>
      <bibl>My bibliographic reference</bibl>
    </quote>
  </cit>
..

back
~~~~

.. _bibliography:

Bibliographie
^^^^^^^^^^^^^

XPath :

-  Section générale bibliographie : ``/TEI/text/back/div[@type="bibliography"]``

-  Sous-sections bibliographiques (relativement à une section
   bibliographique) : ``./listBibl[@type="sectionN"]`` (N compris entre 1 et
   6)

-  Titre de section (relativement à une section bibliographique) : ``./head``

-  Référence bibliographique (relativement à une section bibliographique) : ``./bibl``

Recommandations d’usage :

-  Même principe d’organisation de sections et sous-section que pour `la
   structure du texte <#body-structure>`__ en remplaçant les éléments
   ``div`` par des élements ``listBibl`` et les éléments ``p`` par des élements ``bibl``

Exemple : 

.. code-block:: xml

  <back>
    <div type="bibliography">
     <listBibl type="section1">
      <head>Partie 1</head>
      <listBibl type="section2">
       <head>Partie 1.1</head>
       <bibl>Bennett, Francis et Michael Holdsworth.<hi rend="italic">Embracing the Digital Age. An Opportunity for Booksellers and the Book Trade</hi>. Londres : The Booksellers Association of the United Kingdom &amp; Ireland, 2007</bibl>
       <bibl>Carrérot, Olivier, éd. <hi rend="italic">Qu’est-ce qu’un livre aujourd’hui ? Pages, marges, écrans</hi>. Les Cahiers de la Librairie. Paris : La Découverte, 2009</bibl>
      </listBibl>
      <listBibl type="section2">
       <head>Partie 1.2</head>
       <bibl>Chartier, Roger. « La mort du livre ? », <hi rend="italic">Communication &amp; Langages,</hi> 159 (mars 2009) : 57-66</bibl>
       <bibl>Clémente, Hélène, et Caroline C. Tachon. <hi rend="italic">Accueillir le numérique ? Une mutation pour la
              librairie et le commerce du livre.</hi>
        <ref target="http://www.accueillirlenumerique.com/">http://www.accueillirlenumerique.com/</ref>
       </bibl>
      </listBibl>
     </listBibl>
     <listBibl type="section1">
      <head>Partie 2</head>
      <bibl>Darnton, Robert. « The New Age of the Book », <hi rend="italic" xml:lang="en">The New York Review of Books,</hi> 46, no. 5 (mars 18, 1999)</bibl>
      <bibl>Fogel, Jean-François <hi rend="italic">Une presse sans Gutenberg. Pourquoi Internet a bouleversé le journalisme</hi> Paris : Points, 2007</bibl>
     </listBibl>
    </div>
   </back>
..

Annexes
^^^^^^^

XPath :

-  Section annexes : ``/TEI/text/back/div[@type="appendix"]``

Recommandations d’usage :

-  Tous les éléments utilisables dans ``body`` sont utilisables dans cette
   section (``div``, ``figure``, ``cit``, etc.)

Exemple : 

.. code-block:: xml

  <back>
   <div type="appendix">
    <div type="section1">
     <head>Vivamus laoreet</head>
     <p> Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet
  ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce
  neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus,
  vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum
  odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent
  ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas
  volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut
  urna cursus vestibulum.
     <hi rend="italic">Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio.</hi>
     </p>
     <div type="section2">
      <head>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</head>
      <p> Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
  dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque
  eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla
  vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis
  vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras
  dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo
  ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus
  in, viverra quis, feugiat a, tellus.
      </p>
      <figure>
       <head>Titre de la figure</head>
       <graphic url="relative/path/to/image/img-1.jpg"/>
       <p rend="caption">Schéma réalisé en septembre 2009</p>
       <p rend="credits">Surletoit</p>
      </figure>
      <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper
  ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum
  rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam
  nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante
  tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
  Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit
  amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales,
  augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend
  sapien.</p>
      <cit>
       <quote>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. N</quote>
      </cit>
      <p>Nunc nonummy metus.</p>
     </div>
    </div>
   </div>
  </back>
..
