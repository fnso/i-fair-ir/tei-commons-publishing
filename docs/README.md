# Using Sphinx documentation

## Install

Install Sphinx : https://www.sphinx-doc.org/en/master/usage/installation.html

Install rtd theme : https://sphinx-rtd-theme.readthedocs.io/en/stable/installing.html

Install on Linux :

``` 
sudo apt-get install python3-sphinx
pip install sphinx_rtd_theme
```

## Get the project

```
git clone git@git.unicaen.fr:fnso/i-fair-ir/tei-commons-publishing.git
cd tei-commons-publishing
git checkout doc_usages_rst
```

## Write the documentation in rst

https://www.sphinx-doc.org/fr/master/usage/restructuredtext/index.html

## Generate documentation

```
cd docs/
rm -rf build/html # sometime usefull
make html # generate html documentation in docs/build/html
firefox build/html/index.html # see the result
```
